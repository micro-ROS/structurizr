import com.structurizr.Workspace;
import com.structurizr.api.StructurizrClient;
import com.structurizr.documentation.Format;
import com.structurizr.documentation.StructurizrDocumentationTemplate;
import com.structurizr.model.Location;
import com.structurizr.model.Model;
import com.structurizr.model.Person;
import com.structurizr.model.SoftwareSystem;
import com.structurizr.model.Tags;
import com.structurizr.model.Container;
import com.structurizr.model.Component;
import com.structurizr.model.DeploymentNode;
import com.structurizr.model.Element;
import com.structurizr.view.*;

import com.structurizr.io.plantuml.PlantUMLWriter;
import java.io.FileWriter;
import java.io.StringWriter;

import com.structurizr.util.MapUtils;
/**
 * This is a simple example of how to get started with Structurizr for Java.
 */
public class Structurizr {

    private static final long WORKSPACE_ID = 0;
    private static final String API_KEY = "";
    private static final String API_SECRET = "";
    
    private static final String EXISTING_SYSTEM_TAG = "Existing System";
    private static final String NODE_VIEW_TAG = "NodeView";
    private static final String TIME_VIEW_TAG = "TimeView";
    private static final String SERVICES_VIEW_TAG = "ServicesView";
    private static final String PARAMETER_VIEW_TAG = "ParameterView";
    private static final String GRAPH_VIEW_TAG = "GraphView";
    private static final String LIFE_VIEW_TAG = "LifeView";
    private static final String LOG_VIEW_TAG = "LogView";

    public static void main(String[] args) throws Exception {

        Workspace workspace = new Workspace("micro-ROS", "This is a model of micro-ROS platform.");
        Model model = workspace.getModel();

        // add some elements to your software architecture model
        // Persons and Software systems
        Person user = model.addPerson("User", "A user of ROS2-micro-ROS systems.");
        Person developer = model.addPerson("Developer", "A micro-ROS user application developer.");

        SoftwareSystem ROS2app = model.addSoftwareSystem(Location.External, "ROS2 App", "ROS2 application.");
        ROS2app.addTags(EXISTING_SYSTEM_TAG);
        user.uses(ROS2app, "Uses");

        SoftwareSystem microROS = model.addSoftwareSystem("micro-ROS", "micro-ROS platform providing ROS2 functionality on MCU");
        developer.uses(microROS, "Uses");
        microROS.uses(ROS2app, "Communicates", "DDS");

        SoftwareSystem microROSapp = model.addSoftwareSystem(Location.External, "micro-ROS Application", "User application developed using micro-ROS.");
        microROSapp.addTags(EXISTING_SYSTEM_TAG);
        microROSapp.uses(microROS, "Uses");
        user.uses(microROSapp, "Uses");
        developer.uses(microROSapp, "Delivers");
        
        SoftwareSystem os = model.addSoftwareSystem(Location.External, "OS", "Operative system");
        os.addTags(EXISTING_SYSTEM_TAG);
        microROS.uses(os, "Uses");  
                
        // Containers
        Container microROSStack = microROS.addContainer("micro-ROS Stack", "MicroROS Stack providing ROS2 concepts and communications", "C99");
        Container microROSAgent = microROS.addContainer("micro-ROS Agent", "micro-ROS Agent acting as server for micro-ROS Clients. It provides ROS2 communication to micro-ROS Clients", "C++");

        developer.uses(microROSStack, "Uses");
        microROSapp.uses(microROSStack, "Uses");
        microROSAgent.uses(ROS2app, "Sends Messages", "DDS");
        ROS2app.uses(microROSAgent, "Sends Messages", "DDS");
        microROSAgent.uses(microROSStack, "Sends Messages", "XRCE-DDS");
        microROSStack.uses(microROSAgent, "Sends Messages", "XRCE-DDS");
        microROSAgent.uses(os, "Uses");
        microROSStack.uses(os, "Uses");

        // Components
        // micro-ROS library
        Component node = microROSStack.addComponent("Node", "Main concept on ROS2 used to handle rest of functionality", "C99");
        node.addTags(NODE_VIEW_TAG, TIME_VIEW_TAG, SERVICES_VIEW_TAG, PARAMETER_VIEW_TAG, GRAPH_VIEW_TAG, LOG_VIEW_TAG);        
        Component publisher = microROSStack.addComponent("Publisher", "Allows the user to serialize and sent messages", "C99");
        publisher.addTags(SERVICES_VIEW_TAG, PARAMETER_VIEW_TAG, GRAPH_VIEW_TAG, LIFE_VIEW_TAG);
        Component subscription = microROSStack.addComponent("Subscriber", "Allows the user to receibe and deserialize messages", "C99");
        subscription.addTags(TIME_VIEW_TAG, SERVICES_VIEW_TAG, GRAPH_VIEW_TAG);
        Component service_client = microROSStack.addComponent("Service Client", "Provides service consumer utilities", "C99");
        service_client.addTags(SERVICES_VIEW_TAG, PARAMETER_VIEW_TAG, GRAPH_VIEW_TAG);
        Component service_server = microROSStack.addComponent("Service Server", "Provides service server utilities", "C99");
        service_server.addTags(SERVICES_VIEW_TAG, PARAMETER_VIEW_TAG, GRAPH_VIEW_TAG, LIFE_VIEW_TAG);
        Component timer = microROSStack.addComponent("Timer utilities", "Provides the user with deterministic timer interface", "C99");
        timer.addTags(TIME_VIEW_TAG);
        Component clock = microROSStack.addComponent("Clocks", "Time utilities to control time", "C99");
        clock.addTags(TIME_VIEW_TAG);
        Component parameters = microROSStack.addComponent("Parameters manager", "Provides acces, modification and handles events for parameters. Parameters are stored on a remote server", "C99");
        parameters.addTags(PARAMETER_VIEW_TAG);
        Component executors = microROSStack.addComponent("Executor", "Allows to run nodes and communications", "C99");
        executors.addTags(NODE_VIEW_TAG);
        Component graph_manager = microROSStack.addComponent("Graph manager", "Provides and interface to query graph and subscribe to graph changes", "C99");
        graph_manager.addTags(GRAPH_VIEW_TAG);
        Component logger = microROSStack.addComponent("Logging utilities", "Node named loggers", "C99");
        logger.addTags(LOG_VIEW_TAG);
        Component lifecycle = microROSStack.addComponent("Lifecycle", "Nodes managed by a FSM ", "C99");
        lifecycle.addTags(LIFE_VIEW_TAG);

        // micro-ROS agent
        Component graph_server = microROSAgent.addComponent("Graph server", "Node graph status services", "C++11");
        Component agent_core = microROSAgent.addComponent("Agent Core", "Main communications funcionatility", "C+11");
        Component parameters_server = microROSAgent.addComponent("Parameters server", "Centrilized parameters source. Acts as mirror of local parameters", "C++11");
        
        //Components relations
        // for (Component component : microROSStack.getComponents())
        // {
        //     // developer.uses(component, "Uses");
        //     microROSapp.uses(component, "Uses");
        // }
        microROSapp.uses(executors, "Uses");
        microROSapp.uses(node, "Uses");
        microROSapp.uses(publisher, "Uses");
        microROSapp.uses(subscription, "Uses");
        microROSapp.uses(service_client, "Uses");

        executors.uses(node, "Uses", "C99");

        node.uses(publisher, "Uses", "C99");
        node.uses(subscription, "Uses", "C99");
        node.uses(service_client, "Uses", "C99");
        node.uses(service_server, "Uses", "C99");
        node.uses(parameters, "Uses", "C99");
        node.uses(timer, "Uses", "C99");
        node.uses(graph_manager, "Uses", "C99");
        node.uses(logger, "Uses", "C99");

        graph_manager.uses(service_client, "Graph request", "C99");
        subscription.uses(graph_manager, "Graph callback", "C99");

        publisher.uses(microROSAgent, "Sends Messages", "XRCE-DDS");
        subscription.uses(microROSapp, "Callback", "C99");
        microROSAgent.uses(subscription, "Sends Messages", "XRCE-DDS");
        service_client.uses(publisher, "Requests", "C99");
        subscription.uses(service_client, "Responses", "C99");
        service_server.uses(publisher, "Responses", "C99");
        subscription.uses(service_server, "Request", "C99");
        service_server.uses(microROSapp, "Service Callback", "C99");
        timer.uses(microROSapp, "Timer Calback", "C99");   
        timer.uses(os, "OS/HW timer", "C99") ;
        timer.uses(clock, "ROS timer", "C99");

        parameters.uses(microROSapp, "Parameter Callback", "C99");
        parameters.uses(publisher, "Parameter event", "C99");
        parameters.uses(service_client, "Parameter request", "C99");
        service_server.uses(parameters, "Parameter request", "C99");
        
        clock.uses(os, "OS clock", "C99");
        subscription.uses(clock, "Clock callback", "C99");

        lifecycle.uses(publisher, "State Event", "C99");
        lifecycle.uses(microROSapp, "State callback", "C99");
        service_server.uses(lifecycle, "States services", "C99");

        logger.uses(microROSapp, "Logger Writer callback", "C99");

        // micro-ROS Agent relations      
        agent_core.uses(microROSStack, "Sends Messages", "XRCE-DDS");
        agent_core.uses(ROS2app, "Sends Messages", "DDS");
        ROS2app.uses(agent_core, "Sends Messages", "DDS");
        microROSStack.uses(agent_core, "Sends Messages", "XRCE-DDS");
        
        graph_server.uses(agent_core, "Servive Responses", "DDS");
        agent_core.uses(graph_server, "Service Requests", "DDS");

        parameters_server.uses(agent_core, "Parameters", "DDS");
        agent_core.uses(parameters_server, "Parameters", "DDS");
        
        // deployment nodes and container instances
        DeploymentNode ros2computer = model.addDeploymentNode("Computer", "A general purpose computer", "Linux or Microsoft Windows 10 or Apple macOS");
        DeploymentNode ros2 = ros2computer.addDeploymentNode("ROS2 environment", "ROS2 environment", "ROS2");
        DeploymentNode microcontroller = model.addDeploymentNode("Microcontroller based platform", "A STM32 based microcontroller board", "STM32");
        DeploymentNode nuttX = microcontroller.addDeploymentNode("RTOS", "A RTOS", "NuttX");
        nuttX.add(microROSStack);
        ros2.add(microROSAgent);
        
 

        // // layered components
        // Component RCLC = microROSStack.addComponent(
        //     "RCLC layer", "High level user interface. Exposes all the ROS2 concepts and functionalities. Implementation dependent on target language.", "C99");
        // microROSapp.uses(RCLC, "Uses", "C interface");
        // Component RCL = microROSStack.addComponent(
        //     "RCL layer", "Provides ROS2 services common language independent. Provides time, login, name handling apart from interfacing with middle-ware layer directly.", "C99");
        // RCLC.uses(RCL, "Uses", "C interface");
        // Component RMW = microROSStack.addComponent(
        //     "RMW layer", "Provides an abstract interface of message interchange to the upper layer. Provides publish/subscribe, services and serialization mechanism.", "C99");
        // RCL.uses(RMW, "Uses", "C interface");
        // Component RMW_impl = microROSStack.addComponent(
        //     "RMW implementation", "Middle-ware specific implementation. As long as it provides means to implement RMW interface any implementation/protocol can be used.", "C99");
        // RMW.uses(RMW_impl, "Uses", "C interface");
        // Component RTOS_abstraction = microROSStack.addComponent(
        //     "RTOS abstractions", "Provides abstract interface to Middleware layer. Provides a common interface for supporting multiple RTOS's.", "C99");
        // RMW_impl.uses(RTOS_abstraction, "Uses", "C interface");
        // RMW_impl.uses(ROS2app, "Communicates", "XRCE-DDS");

        // Component RTOS = microROSStack.addComponent(
        //     "RTOS", "Real time operative system. Provides common services and a real time infrastructure.", "OS");
        // RTOS.addTags(EXISTING_SYSTEM_TAG);
        // RTOS_abstraction.uses(RTOS, "Uses", "OS API");

        // Component hardware = microROSStack.addComponent(
        //     "Hardware", "Low resources micro-controller.", "STM32");
        // hardware.addTags(EXISTING_SYSTEM_TAG);
        // hardware.uses(hardware, "Uses");

        // define some views (the diagrams you would like to see)
        ViewSet views = workspace.getViews();
        SystemContextView contextView = views.createSystemContextView(microROS, "SystemContext", "System Context diagram.");
        contextView.addAllSoftwareSystems();
        contextView.addAllPeople();

        ContainerView containerView = views.createContainerView(microROS, "Containers", "The Container diagram for the micro-ROS platform.");
        containerView.addAllPeople();
        containerView.addAllSoftwareSystems();
        containerView.addAllContainers();

        ComponentView microROSClientView = views.createComponentView(microROSStack, "micro-ROS Stack", "The component diagram for the micro-ROS Stack.");
        microROSClientView.addAllElements();

        ComponentView microROSClientLog = views.createComponentView(microROSStack, "micro-ROS Stack - Log View", "The component diagram for the micro-ROS Stack - Log view.");
        microROSClientLog.addAllElements();
        for (Component c : microROSStack.getComponents())
        {
            if (!c.getTagsAsSet().contains(LOG_VIEW_TAG))
            microROSClientLog.remove(c);
        }


        ComponentView microROSClientGraph = views.createComponentView(microROSStack, "micro-ROS Stack - Graph View", "The component diagram for the micro-ROS Stack - Graph view.");
        microROSClientGraph.addAllElements();
        for (Component c : microROSStack.getComponents())
        {
            if (!c.getTagsAsSet().contains(GRAPH_VIEW_TAG))
            microROSClientGraph.remove(c);
        }

        ComponentView microROSClientParameter = views.createComponentView(microROSStack, "micro-ROS Stack - Parameter View", "The component diagram for the micro-ROS Stack - Parameter view.");
        microROSClientParameter.addAllElements();
        for (Component c : microROSStack.getComponents())
        {
            if (!c.getTagsAsSet().contains(PARAMETER_VIEW_TAG))
            microROSClientParameter.remove(c);
        }


        ComponentView microROSClientNode = views.createComponentView(microROSStack, "micro-ROS Stack - Node View", "The component diagram for the micro-ROS Stack - Node view.");
        microROSClientNode.addAllElements();
        for (Component c : microROSStack.getComponents())
        {
            if (!c.getTagsAsSet().contains(NODE_VIEW_TAG))
            microROSClientNode.remove(c);
        }

        ComponentView microROSClientViewTime = views.createComponentView(microROSStack, "micro-ROS Stack - Time View", "The component diagram for the micro-ROS Stack - Node view.");
        microROSClientViewTime.addAllElements();
        for (Component c : microROSStack.getComponents())
        {
            if (!c.getTagsAsSet().contains(TIME_VIEW_TAG))
                microROSClientViewTime.remove(c);
        }
        
        ComponentView microROSClientServices = views.createComponentView(microROSStack, "micro-ROS Stack - Services View", "The component diagram for the micro-ROS Stack - Services view.");
        microROSClientServices.addAllElements();
        for (Component c : microROSStack.getComponents())
        {
            if (!c.getTagsAsSet().contains(SERVICES_VIEW_TAG))
            microROSClientServices.remove(c);
        }

        ComponentView microROSAgentView = views.createComponentView(microROSAgent, "micro-ROS agent", "The component diagram for the micro-ROS Agent.");
        microROSAgentView.addAllElements();
            
        DeploymentView developmentDeploymentView = views.createDeploymentView(microROS, "Deployment", "An example deployment scenario for the micro-ROS platform.");
        developmentDeploymentView.addAllDeploymentNodes();

        
        // // add some documentation
        // StructurizrDocumentationTemplate template = new StructurizrDocumentationTemplate(workspace);
        // template.addContextSection(softwareSystem, Format.Markdown,
        //         "Here is some context about the software system...\n" +
        //                 "\n" +
        //                 "![](embed:SystemContext)");

        // colours, shapes and other diagram styling
        Styles styles = views.getConfiguration().getStyles();
        styles.addElementStyle(Tags.ELEMENT).color("#ffffff");
        styles.addElementStyle(Tags.SOFTWARE_SYSTEM).background("#1168bd");
        styles.addElementStyle(Tags.CONTAINER).background("#438dd5");
        styles.addElementStyle(Tags.COMPONENT).background("#85bbf0").color("#000000");
        styles.addElementStyle(Tags.PERSON).background("#08427b").shape(Shape.Person).fontSize(22);
        styles.addElementStyle(EXISTING_SYSTEM_TAG).background("#999999");
        styles.addRelationshipStyle(Tags.RELATIONSHIP).routing(Routing.Orthogonal);
        

        if (WORKSPACE_ID != 0 && !API_KEY.equals("key") && !API_SECRET.equals("secret"))
        {
            uploadWorkspaceToStructurizr(workspace);
        }
        if (args.length == 1)
        {
            writeUML(workspace, args[0]);
        }
    }

    private static void uploadWorkspaceToStructurizr(Workspace workspace) throws Exception {
        StructurizrClient structurizrClient = new StructurizrClient(API_KEY, API_SECRET);
        structurizrClient.putWorkspace(WORKSPACE_ID, workspace);
    }


    private static void writeUML(Workspace workspace, String file) throws Exception {
        FileWriter outputFile = new FileWriter(file);
        PlantUMLWriter plantUMLWriter = new PlantUMLWriter();
        plantUMLWriter.addSkinParam("rectangleFontColor", "#ffffff");
        plantUMLWriter.addSkinParam("rectangleStereotypeFontColor", "#ffffff");
        plantUMLWriter.write(workspace, outputFile);
        outputFile.close();
    }
}